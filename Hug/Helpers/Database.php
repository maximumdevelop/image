<?php
/**
 * @Author Mostafa Naguib.
 * @Copyright Maximum Develop
 * @FileCreated: 12/19/18 1:21 PM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */
use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;


if((getenv('Database_type') == 'sqlite') && isset($_SERVER['REQUEST_METHOD']) && !file_exists(app_base('../database/db.sqlite'))){
    die('DB not found, Kindly do a "composer install" through terminal');
}


switch(getenv('Database_type')) {
    case 'mysql':
        $connection = [
            "driver"        =>  "mysql",
            "host"          =>  getenv('Database_host'),
            "database"      =>  getenv('Database_name'),
            "username"      =>  getenv('Database_username'),
            "password"      =>  getenv('Database_password'),
        ];
    break;
    default:
    case 'sqlite':
        $connection = [
            'driver' => 'sqlite',
            'database' => app_base('../database/db.sqlite'),
        ];
    break;

}


$capsule->addConnection($connection);
$capsule->setAsGlobal();
$capsule->bootEloquent();
